const form = document.forms[0];
const cardNumberInput = form.number;
const cardDateInput = form.date;
const cvvInput = form.cvv;

cardNumberInput.addEventListener('blur', () => refreshCardData('number'));
cardDateInput.addEventListener('blur', () => refreshCardData('date'));
cvvInput.addEventListener('blur', () => refreshCardData('cvv'));

form.addEventListener('submit', (e) => {
  e.preventDefault();
  if (validateAll()) {
    fetchCardData();
    window.location.href = "/lk";
  }
})

const cardData = {
  number: null,
  date: null,
  cvv: null,
}

const tooltipData = {
  'cardNumberLengthError' : 'Поле должно содержать 16 цифр',
  'cvvLengthError' : 'Поле должно содержать 3 цифры',
  'dateLengthError' : 'Номер месяца и номер года должны иметь двухзначный числовой формат',
  'monthError' : 'Некорректное значение месяца',
  'yearError' : 'Некорректное значение года'
}

function fetchCardData() {
  const options = {
    method: 'PUT',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      date: cardData.date,
      number: cardData.number,
      cvv: cardData.cvv
    })
  }
  fetch('/api/card-requisites', options)
    .then(response => response.json())
    .then(data => console.log(data))
    .catch(error => console.error(error))
}

function refreshCardData(field) {
  if (field === 'number') {
    const validCardNumber = cardNumberValidate(cardNumberInput.value.split(' ').join('')); 
    if (validCardNumber) {
      cardData.number = validCardNumber;
      cardNumberInput.value = validCardNumber;
      return true;
    }
  } else if (field === 'date') {
    const validCardDate = cardDateValidation(cardDateInput.value.trim());
    if (validCardDate) {
      cardData.date = validCardDate;
      cardDateInput.value = validCardDate;
      return true;
    }
  } else if (field === 'cvv') {
    const validCvv = cvvValidate(cvvInput.value.trim());
    if (validCvv) {
      cardData.cvv = validCvv;
      return true;
    }
  }
  return false;
}

function validateAll() {
  return refreshCardData('number') && refreshCardData('date') && refreshCardData('cvv');
}

function cardNumberValidate(cardNumberValue) {
  hideError(cardNumberInput);
  if (cardNumberValue === '') {
    return false;
  }

  if (cardNumberValue.length !== 16 || isNaN(cardNumberValue)) {
    renderError('cardNumberLengthError', cardNumberInput);
    return false;
  }

  return cardNumberValue.split('').reduce((acc, char, i) => {
    if (i !== 0 && i % 4 === 0) {
      acc += ' ';
    }
    return acc += char;
  }, '');
}

function cardDateValidation(cardDateValue) {
  hideError(cardDateInput);
  if (cardDateValue === '') {
    return false;
  }

  const normalizeCardDateValue = cardDateValue.split('').reduce((acc, char) => {
    return !isNaN(char) ? acc += char : acc;
  }, '');

  if (normalizeCardDateValue.length !== 4) {
    renderError('dateLengthError', cardDateInput);
    return false;
  }

  const month = normalizeCardDateValue.slice(0,2);
  const year = normalizeCardDateValue.slice(2);

  if (Number(month) > 12 || Number(month) < 1) {
    renderError('monthError', cardDateInput);
    return false;    
  }

  const currentYear = String(new Date().getFullYear()).slice(2);
  if (Number(year) < Number(currentYear) || Number(year) > Number(currentYear) + 5) {
    renderError('yearError', cardDateInput);
    return false;   
  }

  return `${month}/${year}`;
}

function cvvValidate(cardCvvValue) {
  hideError(cvvInput);
  if (cardCvvValue === '') {
    return false;
  }

  if (cardCvvValue.length !== 3  || isNaN(cardCvvValue)) {
    renderError('cvvLengthError', cvvInput);
    return false;
  }
  return cardCvvValue;
}

function renderError(tooltipDataKey, elem) {
  const parent = elem.closest('.input-wrapper');
  const tooltip = document.createElement('div');
  tooltip.classList.add('tooltip');
  tooltip.textContent = tooltipData[tooltipDataKey];
  parent.appendChild(tooltip);

  elem.classList.add('form__input--error');
}

function hideError(elem) {
  const tooltip = elem.nextElementSibling;
  if (tooltip) {
    tooltip.remove();
  }
  elem.classList.remove('form__input--error');
}

