const returnBtns = document.querySelectorAll('.return-btn');
returnBtns.forEach(btn => btn.addEventListener('click', () => deleteOrder(btn)));

function deleteOrder(btn) {
  fetch(`/api/order/${btn.dataset.orderId}`, { method: 'DELETE' })
  .then(response => response.text())
  .then(() => deleteElement(btn))
  .catch(error => console.error(error))
}

function deleteElement(elem) {
  const bikeItem = elem.closest('.bike');
  bikeItem.remove();
}