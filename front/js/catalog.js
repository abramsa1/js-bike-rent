'use strict';
const loadMoreBtn = document.querySelector('#loadMore button');
function init() {
  let page = 1;
  loadCatalog(page);
  
  loadMoreBtn.addEventListener('click', async () => {
    page++;
    disableButtonLoadMore();
    await loadCatalog(page);
    enableButtonLoadMore();
  })
}

async function loadCatalog(page) {
  const data = await getBikes(getPointId(), page);
  appendCatalog(data.bikesList);
  showButtonLoadMore(data.hasMore);
}

function getBikes(id, page) {
  return fetch(`/api/catalog/${id}?page=${page}`)
    .then(response => response.json())
    .then(data => data)
    .catch(error => console.error(error));
}

function appendCatalog(items) {
  const bikeList = document.getElementById('bikeList');
  items.forEach(bike => {
    const bikeItem = createElementWihAttributes('div', 'bikeItem');
    
    const imgWrap = createElementWihAttributes('div', 'bikeItem__img-block');
    const img = createElementWihAttributes('img', 'bikeItem__img', '', {'src':`../images/${bike.img}`});
    imgWrap.appendChild(img);
    bikeItem.appendChild(imgWrap);
    
    const title = createElementWihAttributes('div', 'bikeItem__title', bike.name);
    bikeItem.appendChild(title);

    const price = createElementWihAttributes('div', 'bikeItem__price', `Стоимость за час - ${bike.cost * 60} ₽`);
    bikeItem.appendChild(price);

    const button = createElementWihAttributes('a', 'btn bikeItem__btn', 'Арендовать', {'href':`/order/${bike._id}`});
    bikeItem.appendChild(button);

    bikeList.appendChild(bikeItem);
  })
}

function createElementWihAttributes(tagName, elementClasses, text = '', attrs = {}) {
  const element = document.createElement(tagName);
  element.className = elementClasses;
  element.textContent = text;
  Object.entries(attrs).forEach(([attrName, value]) => element.setAttribute(attrName, value));
  return element;
}

function showButtonLoadMore(hasMore) {
  if (hasMore) {
    loadMoreBtn.classList.remove('hidden');
  } else {
    loadMoreBtn.classList.add('hidden');
  }
}

function disableButtonLoadMore() {
  loadMoreBtn.setAttribute('disabled', 'disabled');
}

function enableButtonLoadMore() {
  loadMoreBtn.removeAttribute('disabled');
}

function getPointId() {
  const pointId = document.URL.split('/').pop();
  return pointId === 'catalog' ? '' : pointId;
}


document.addEventListener('DOMContentLoaded', init)
