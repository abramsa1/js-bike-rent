ymaps.ready(init);

let map = null;

function init() {
  map = new ymaps.Map("map", {
    center: [55.0297, 82.9205],
    zoom: 11
  });
  showPoints();
}

async function showPoints() {
  const pointsData = await api.getPointers();
  for (const point of pointsData) {
    createPointOnMap(point.coordinates, point.address, point._id);
  }
}

function createPointOnMap(coordinates, address, id) {
  const balloonContent = ymaps.templateLayoutFactory.createClass(
    `<h4 class="baloon__title">Пункт по адресу ${address}</h4>
     <a href="catalog/${id}" class="btn baloon__btn">Выбрать велосипед</a>`
  );

  const geoPoint = new ymaps.Placemark(coordinates, {}, {
    preset: 'islands#blueBicycleIcon',
    balloonContentLayout: balloonContent,
    hideIconOnBalloonOpen: true
  });
  map.geoObjects.add(geoPoint);
}